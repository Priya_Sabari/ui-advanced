showBookedAppointments();
//This function shows booked appointments for doctor
 showBookedAppointments= () =>
{
    let httpReq;
    if(window.XMLHttpRequest)
    {
        httpReq = new XMLHttpRequest();
    }
    else{
         httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }
  // api call response handling 
    httpReq.onreadystatechange = function() 
    {
  if(this.readyState===4 && this.status===200)
  {
      //Create Heading
    const appointmentHeading =  document.createElement("h2");
    appointmentHeading.appendChild(document.createTextNode("Appointments"));
    appointmentHeading.setAttribute("id",'headingID');

    const table = document.createElement('table');
      table.setAttribute("id",'dataTable');
      
      const thead = document.createElement('thead');
      const tbody = document.createElement('tbody');

      const headTr = document.createElement('tr');

      const td1 = document.createElement('td');
      const td1Text = document.createTextNode("Doctor Mail ID");
      td1.appendChild(td1Text);

      const td2 = document.createElement('td');
      const td2Text = document.createTextNode("Date");
      td2.appendChild(td2Text);

      const td3 = document.createElement('td');
      const td3Text = document.createTextNode("Time");
      td3.appendChild(td3Text);

      const td4 = document.createElement('td');
      const td4Text = document.createTextNode("Specialist");
      td4.appendChild(td4Text);

      const td5 = document.createElement('td');
      const td5Text = document.createTextNode("Patient Email");
      td5.appendChild(td5Text);

     
      
      headTr.appendChild(td1);
      headTr.appendChild(td2);
      headTr.appendChild(td3);
      headTr.appendChild(td4);
      headTr.appendChild(td5);
   

      thead.appendChild(headTr);
 
       let data = JSON.parse(this.response);
       const len = data.length;
      if(len>0)
      {
       for(let i=0;i<len;i++)
       {
          
           
        const tbodyTr = document.createElement("tr");
        const td1 = document.createElement("td");
        const td2 = document.createElement("td");
        const td3 = document.createElement("td");
        const td4 = document.createElement("td");
        const td5 = document.createElement("td");


           let td1TextNode = document.createTextNode(data[i].doctoremail);
           let td2TextNode = document.createTextNode(data[i].date);
           let td3TextNode = document.createTextNode(data[i].time);
           let td4TextNode = document.createTextNode(data[i].specialist);
           let td5TextNode = document.createTextNode(data[i].patientemail);
       


           td1.appendChild(td1TextNode);
           td2.appendChild(td2TextNode);
           td3.appendChild(td3TextNode);
           td4.appendChild(td4TextNode);
           td5.appendChild(td5TextNode);      

           tbodyTr.appendChild(td1);
           tbodyTr.appendChild(td2);
           tbodyTr.appendChild(td3);
           tbodyTr.appendChild(td4);
           tbodyTr.appendChild(td5);


           tbody.appendChild(tbodyTr);
         

       }
      }
      else{
        const data = document.createElement("h4");
        const noData = document.createTextNode("No Data Available");
            data.appendChild(noData);
            tbody.appendChild(data);
      }
       
      table.appendChild(thead);
      table.appendChild(tbody);
      

      const body = document.getElementsByTagName('body')[0];
      
      body.appendChild(appointmentHeading);
      body.appendChild(table);
    
  }
}
let doctoremail = sessionStorage.getItem('${user}');
//string literal
httpReq.open('GET','http://localhost:3000/appointment?doctoremail=${doctoremail}');
httpReq.send();
}





