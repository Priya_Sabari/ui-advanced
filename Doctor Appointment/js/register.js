// this method is used to create the user based on the role 
  createUser= () => {
    //validating user name here
    const name = document.getElementById("uname").value;
    if (name === "" && name.length === 0) {
        alert("Name must be filled");
        return false;
    }
   

    //code that allows only dbs  or hcl emails 
    const email = document.getElementById("email").value;
    const mailformat = "[a-z0-9._%+-]+@[hcl|dbs]+\.[a-z]{2,}$";
    if (email.match(mailformat)) {
    }
    else {
        alert("Enter vallid email id...(Use only DBS or HCL Domain)");
        return false;
    }
     /* phone number validation */
     const phone = document.getElementById("phone").value;
     if (phone.length === 10) {
     }
     else {
         alert("Phone number must be filled with 10 digits")
         return false;
     }
     const role = document.getElementById("role").value;
     if (role === "" && role.length === 0) {
         alert("role must be selected");
         return false;
     }
       /* creating user object */
       console.log("validation done");


       // logic to generate password 
    let length = 8,
        charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789",
         password = "";
    for (let i = 0, n = charset.length; i < length; ++i) {
        password += charset.charAt(Math.floor(Math.random() * n));
    }
    console.log("password created")

    //shorthand properties
    let user = {name,email,phone,role,password};
    console.log(user);


    let httpRequest;

    if (window.XMLHttpRequest) {
    httpRequest = new XMLHttpRequest();
      }
    else {
    httpRequest = new ActiveXObject();
      }

 // api call response handling 
    httpRequest.onreadystatechange =  function() {
        if (this.status === 201 && this.readyState === 4) {
            window.alert("user added, please login!");
            window.location.assign("login.html");
        }
    }
    jsonUser = JSON.stringify(user);
  

    console.log(jsonUser);

       /* api call to  send the user data to server */
    const url='http://localhost:3000/users';
    httpRequest.open('POST',url,true);
    httpRequest.setRequestHeader('Content-type', 'application/json; charset=utf-8');
    httpRequest.send(jsonUser);
    
   
}
//This function checks for the user existance and if the user doesn't exist the user gets registered
 checkAndRegisterUser = () =>
{
    
    const email = document.getElementById("email").value;

    let httpReq;
    if(window.XMLHttpRequest)
    {
    httpReq = new XMLHttpRequest();
    }
    else{
    httpReq  = new ActiveXObject("Microsoft.XMLHTTP");
    }

 // api call response handling 
    httpReq.onreadystatechange = function() 
    {
        if(this.readyState===4 && this.status===200)
        {
            let data = JSON.parse(this.response);
            const len = data.length;
            if(len>0)
            {
                alert("User with given email already exists.");
                return false;
            }
            else
            createUser();
        }
    } 
    
    //string literal
    httpReq.open('GET','http://localhost:3000/users?email=${email}');
    httpReq.send();

}
